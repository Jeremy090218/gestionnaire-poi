import {Injectable} from "@angular/core";
import {Poi} from "../model/poi.model";
import {Storage} from "@ionic/storage";
import {ToastController} from "ionic-angular";
    
@Injectable()
export class PoiService {
    private lesPois: Array<Poi> = [];
    
    constructor(
        public toastCtrl: ToastController,
        public storage: Storage   
    ) {}
    
    addPoi(unPoi: Poi) {
        this.lesPois.push(unPoi);
        this.savePois();
    }

    deletePoi(unPoi: Poi) {
        var index = this.lesPois.indexOf(unPoi);
        this.lesPois.splice(index,1);
        this.savePois();
    }

    getAllPois() {
        return this.lesPois;
    }

    updatePoi(unPoi: Poi) { // Pour mettre à jour un POI qui a été modifié
        for (let i = 0; i < this.lesPois.length; i++) { // on recherche le Poi modifié
            if (this.lesPois[i].timeStamp== unPoi.timeStamp) {
                this.lesPois[i] = unPoi;
                return this.savePois(); // Renvoie une promesse booléenne 
            }
        }
    }
    
    loadPois() { // Charge les Pois et renvoie une promesse booléenne (succes/echec)
        return this.storage.get("lesPois") // On essaye de récupérer la variable lesPois dans le storage
        .then(data => {  // Quand succes chargement
            this.lesPois= (data != null) ? (data as Array<Poi>) : []; // on copie le tableau lu dans this.lesPois
            var message: string = this.lesPois.length ? this.lesPois.length+ " Pois Chargés" : "Pas de Pois enregistrés"
            this.toastCtrl.create({message: message, duration: 2000, position: 'bottom'}).present();
            return new Promise<boolean> (function(resolve) {
                resolve(true);
            });
        }).catch(reason => { // Quand échec chargement
            console.log("Echec Storage Get : " + reason); // on logge la raison de l'échec
            this.toastCtrl.create({message: 'Echec Lecture Pois', duration: 2000, position: 'bottom'}).present();
            this.lesPois= [] as Array<Poi>;
            return new Promise(function(resolve, reject) {
                reject(reason);
            });
        });
    }

    savePois() { // Sauvegarde le tableau des Pois et renvoie une promesse booléenne (succes/echec)
        // pour pouvois accéder aux attributs du service dans la promesse
        var storage = this.storage;
        var lesPois = this.lesPois;
        var toastCtrl = this.toastCtrl;
        return new Promise<boolean>(function(resolve, reject) {
            storage.set("lesPois", lesPois) // on réécrit tous les Pois dans le storage (pas efficace !)
            .then(data => {     // Quand succes
                toastCtrl.create({message: lesPois.length+ ' Pois Enregistrés',duration: 2000,position: 'bottom'}).present();
                resolve(true);  // On résoud la promesse
            })
            .catch(reason => {  // Quand échec
                console.log("Echec Storage Set : " + reason); // on logge la raison de l'échec
                toastCtrl.create({message: 'Echec Enregistrement Pois',duration: 2000, position: 'bottom'}).present();
                reject(false); // La promesse n'est pas tenus
            });
        });
    }
}