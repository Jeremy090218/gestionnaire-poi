Développement en solo. Projet de Licence Pro Applications Web. A

pplication multi-plateformes de gestion de Points Of Interest en Ionic. 
Gestion du stockage local (ajouts, modifs, suppressions), utilisation d'une API REST afin d'obtenir des images selon un POI recherché, gestion de l'adaptabilité entre les plateformes.

Pour tester le projet :

- Télécharger les fichiers
- A la racine du dossier en local, taper la commande "npm install"
- Pour lancer l'application sur navigateur, taper "ionic cordova run browser". Pour un test sur Android, saisir "ionic cordova run android".
