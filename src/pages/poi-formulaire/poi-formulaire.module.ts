import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PoiFormulairePage } from './poi-formulaire';

@NgModule({
  declarations: [
    PoiFormulairePage,
  ],
  imports: [
    IonicPageModule.forChild(PoiFormulairePage),
  ],
})
export class PoiFormulairePageModule {}
