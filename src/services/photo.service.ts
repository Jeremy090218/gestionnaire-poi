import {Injectable} from"@angular/core";
import {HTTP} from"@ionic-native/http";

@Injectable() 
export class PhotoService {
    // Service pour se connecter au Web Service Rest Pixabay
    // Paramètres du Web Service Pixabay
    site: string = "https://pixabay.com/api/";                
    // Site Pixabay
    key: string = "9024587-558fbc19c806c99a109a2c431";        
    // Clé PhM pour accéder à l'API
    options: string = "&image_type=photo&per_page=10&page=1"; // Options de la requête : max 10 photos

    constructor(public http: HTTP) { // on injecte le composant HTTP
    }

    getPhotosVille(ville: string) {
        let url: string = this.site+ "?key=" + this.key+ this.options+ "&q=" + encodeURI(ville); // On forge l'URL
        let http: HTTP = this.http; // On rend le service accessible dans la promesse
        return new Promise(function(resolve, reject) {http.get(url, {}, {}) // On lance la requête HTTP7
            .then(response => {          // Quand on a une réponse
                resolve(JSON.parse(response.data)); // On résoud la promesse avec l'objet reçu après conversion JSon -> JS
            }).catch(reason => { // Quand pas de réponse
                console.log("Echec HTTP : "+reason);  // On logue la raison de l'échec
                reject(reason);                       // On transmet la raison de l'échec
            });
        });
    }
}