import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Poi } from '../../Model/poi.model';
import { PoiService } from '../../services/poi.service';

@IonicPage()
@Component({
  selector: 'page-poi-details',
  templateUrl: 'poi-details.html',
})
export class PoiDetailsPage {
  private poi: Poi;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public poiService: PoiService) {
    this.poi = this.navParams.get("poi");
    console.log(this.poi);
  }

  supprimerPoi() {
    this.poiService.deletePoi(this.poi);
    this.navCtrl.pop();
  }
}
