import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { User } from"../../model/user.model";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private user: User;

  constructor(public navCtrl: NavController) {

  }

  ionViewDidLoad() {
    this.user = new User("Bob", "assets/imgs/bob.jpg", ["bob_leponge@gmail.com", "squarepants@gmail.com"]);
  }

}
