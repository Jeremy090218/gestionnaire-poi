export class Poi {
    titre: string;
    pays?: string;
    ville?: string;
    timeStamp?: number; // date de création du POI
    motsCles?: string[];
    coordonnees? : {       // les coordonnées lors de la création du POI
        latitude: number,
        longitude: number
    };

    constructor(titre: string,
        pays: string,
        ville: string,
        date: number,
        motsCles: string[]
        ) {
            this.titre = titre;
            this.pays = pays;
            this.ville = ville;
            this.timeStamp = date;
            this.motsCles = motsCles;
    }
}