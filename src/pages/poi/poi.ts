import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Poi } from '../../Model/poi.model';
import { PoiService } from '../../services/poi.service';
import { PoiFormulairePage } from '../poi-formulaire/poi-formulaire';
import { PoiDetailsPage } from '../poi-details/poi-details';

/**
 * Generated class for the PoiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-poi',
  templateUrl: 'poi.html',
})
export class PoiPage {

  private pois: Poi[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public poiService: PoiService,     // Service pour gérer les Pois dans le 'storage'
    public alertCtrl: AlertController) {
    this.pois = [];
  }

  onNewPoi() {
    this.navCtrl.push(PoiFormulairePage); // on empile la page poi-formulaire
  }

  ionViewWillEnter() {
    this.pois = this.poiService.getAllPois();
  }

  ionViewDidLoad() {
    let loading = this.loadingCtrl.create({content: 'Chargement...'});
    loading.present(); // on affiche une alerte
    let navCtrl = this.navCtrl;
    this.poiService.loadPois().then(resultat => {   // Quand le service répond
      this.pois= this.poiService.getAllPois();
      loading.dismiss();           // on cache le modal
    })
    .catch(resultat => {  // Quand le service échoue
      loading.dismiss();           // on cache le modal
      this.alertCtrl.create({      // On affiche une alerte
        title: 'Pas de POIs Enregistrés',subTitle: 'Cliquez sur '+' pour en ajouter',buttons: ['Fermer']
      }).present();
    });
  }

  poiDetails(poi) {
    this.navCtrl.push(PoiDetailsPage, {"poi": poi});
  }
}
