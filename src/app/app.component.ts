import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { PoiPage } from '../pages/poi/poi';
import { PoiFormulairePage } from '../pages/poi-formulaire/poi-formulaire';
import { PhotoPage } from '../pages/photo/photo';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  menus = [
    {titre: "Accueil", composant: HomePage},
    {titre: "Mes POI", composant: PoiPage},
    {titre: "Photos", composant: PhotoPage}
  ];
  
  onPage(menuItem) { // permet de naviguer vers une page du menu
    this.rootPage=menuItem.composant;
  }

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

