import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PoiDetailsPage } from './poi-details';

@NgModule({
  declarations: [
    PoiDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PoiDetailsPage),
  ],
})
export class PoiDetailsPageModule {}
