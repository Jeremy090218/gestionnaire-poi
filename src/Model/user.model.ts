export class User {
    nom: string;
    photo: string;
    mails: string[];
    
    constructor(nom: string, photo: string, mails: string[]) {
        this.nom= nom;
        this.photo= photo;
        this.mails= mails;
    }
    
    addMail(mail: string) {
        this.mails.push(mail);
    }
}