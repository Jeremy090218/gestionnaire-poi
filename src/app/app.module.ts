import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, Injectable } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HTTP } from"@ionic-native/http";
import { PhotoViewer } from '@ionic-native/photo-viewer';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PoiPage } from '../pages/poi/poi';
import { PoiService } from '../services/poi.service';
import { PoiFormulairePage } from '../pages/poi-formulaire/poi-formulaire';
import { IonicStorageModule } from '@ionic/storage';
import { PoiDetailsPage } from '../pages/poi-details/poi-details';
import { PhotoService } from '../services/photo.service';
import { PhotoPage } from '../pages/photo/photo';
import { DetailPhotoPage } from '../pages/detail-photo/detail-photo';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PoiPage,
    PoiFormulairePage,
    PoiDetailsPage,
    PhotoPage,
    DetailPhotoPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({ // on importe le module storage avec une config par défaut
      name: '__voyagedb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    PoiPage,
    PoiFormulairePage,
    PoiDetailsPage,
    PhotoPage,
    DetailPhotoPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PoiService,
    PhotoService,
    HTTP,
    PhotoViewer
  ]
})
export class AppModule {}
