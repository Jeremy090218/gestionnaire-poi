import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { PhotoService } from '../../services/photo.service';
import { DetailPhotoPage } from '../detail-photo/detail-photo';

/**
 * Generated class for the PhotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-photo',
  templateUrl: 'photo.html',
})

export class PhotoPage { // Composant pour afficher les photos d'une ville en utilisant un Web Service
  ville: string; // La ville recherchée
  photos: any;   // L'objet JS renvoyé par l'API Pixabay : cf https://pixabay.com/api/docs/
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public photoService: PhotoService, // Notre service Ionic pour interroger le Web Service Pixabay
    public alertCtrl: AlertController) {

  }
  
  ionViewDidLoad() {    
    // A chaque chargement de la page...
    this.ville= "";    //  On réinitialise la ville
    this.photos= null; //    et les photos associées7
  }
  
  onRecherche() { // Quand on clique sur le bouton recherche
    this.photoService.getPhotosVille(this.ville) // On interroge le service qui renvoie une promesse
    .then(pixabayAnswer => {  // Quand le service a fourni une réponse
      this.photos= pixabayAnswer;      // On mémorise la réponse (objet JS)
      if (this.photos.totalHits == 0) { // Si pas de photos, on affiche une alerte
        this.alertCtrl.create({
          title: 'Aucune Image Trouvée', subTitle: 'Ville Non Référencée', buttons: ['Recommencer']
        }).present();
      }
    })
    .catch(reason => {       // Quand le service n'a pas fourni de réponse
      this.photos= null;                 // On réinitialise les photos
      console.log(reason);                // On logge la raison de l'échec
      this.alertCtrl.create({             // On affiche une alerte
        title: 'Service Photos Indisponible', subTitle: 'Connexion Impossible', buttons: ['Fermer']
      }).present();
    });
  }

  onDetailImage(unePhoto) { // Quand on clique sur une vignette
    this.navCtrl.push(DetailPhotoPage, {photo: unePhoto});  // On navigue vers le détail de l’image
    //  en lui transmettant l'image
  }
}
