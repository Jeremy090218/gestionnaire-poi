import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PhotoViewer } from '@ionic-native/photo-viewer';

/**
 * Generated class for the DetailPhotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-photo',
  templateUrl: 'detail-photo.html',
})

export class DetailPhotoPage {

  photo : any; // un objet JS représentant une image Pixabay

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public photoViewer: PhotoViewer) {}
  
  ionViewDidLoad() { // Au chargement
    this.photo= this.navParams.get('photo'); // on récupère l'image
  }
  
  onZoom() {
    console.log(this.photo.largeImageURL);
    this.photoViewer.show(this.photo.largeImageURL, '', {share:false});
  }
}
