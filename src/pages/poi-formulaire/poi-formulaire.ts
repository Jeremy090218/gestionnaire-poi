import { Component } from'@angular/core';
import { IonicPage, NavController, NavParams } from'ionic-angular';
import { Poi } from"../../model/poi.model";
import { PoiService } from"../../services/poi.service";

@IonicPage()
@Component({
  selector: 'page-poi-formulaire',
  templateUrl: 'poi-formulaire.html',
})

export class PoiFormulairePage {
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public poiService: PoiService) {}
    
    onAddPoi(unPoi: Poi){
      unPoi.timeStamp = new Date().getTime(); // on définit l'heure à maintenant
      this.poiService.addPoi(unPoi);
      this.navCtrl.pop();
    }
  }

